# Penz v0.1.0
#### 功能简介
像绘画一样为模型边界上色  
Paint edges with colors in SketchUp like using a pen 

#### 基本操作
1. 边界涂色，无论是否在群组/组件之内  
draw any edges whether in group\compenent or not  
2. 独立的色彩面板用于添加新材质和管理现有材质（材质名包含 'OnEdge'）  
an independent color panel to add new material and manage the existing edge mateials(name including 'OnEdge')  
3. 双击可以添加或是移除一个端点，同时保持整个直线段不变  
double click to add or delete a vertex keeping the edge the same
4. +/-改变颜色深浅，[/]改变颜色变化速度
+/- to change the color of material. [/] to change the speed of changing the color

![Demo](https://images.gitee.com/uploads/images/2020/0304/160948_5f92d6ea_5749239.gif "penz01_gif.gif")

#### 安装教程
支持SketchUp2017及以上   
下载zhouxi_Penz文件夹以及zhouxi_Penz.rb   
将文件放入SU的默认插件文件夹，一般位于   
>  C:\Users\Administrator\AppData\Roaming\SketchUp\SketchUp 2018\SketchUp\Plugins\

#### 更新支持
![QRCode](https://images.gitee.com/uploads/images/2020/0229/184541_1ee6fd3e_5749239.png "QRcode.png")   
微信公众号：Catching Up    
邮箱：zhou.xiii@qq.com  
***